# **Projet Java PRODUCTEURS / CONSOMMATEURS**

Projet dans le cas du cours de Programmation Concurrente à **POLYTECH GRENOBLE**.

**Groupe composé de:**
* **[BONFILS Antoine](https://gitlab.com/AntoineBF)** (INFO4)
* **[FERRARI Julien](https://gitlab.com/ferrarju)**  (INFO4)

***

## **TABLE DE NAVIGATION**

* [Sujet](PRODCONS/PRODCONS/Projet-ProdCons.pdf)

* [Objectif 1 & 2](PRODCONS/PRODCONS/src/ProdCons/v1_2/) - Solution directe avec Terminaison

* [Objectif 3](PRODCONS/PRODCONS/src/ProdCons/v3/) - Solution basée Sémaphore

* [Objectif 4](PRODCONS/PRODCONS/src/ProdCons/v4/) - Solution basée sur les Locks et Conditions de Java

* [Objectif 5](PRODCONS/PRODCONS/src/ProdCons/v5/) - Multi-consommation

* [Objectif 6](PRODCONS/PRODCONS/src/ProdCons/v6/) - Multi-exemplaires synchrone

***

## **TABLE DE PROGRESSION**

| Questions  | Traitées  |
|:----------:|:---------:|
| Objectif 1 | :heavy_check_mark:  |
| Objectif 2 | :heavy_check_mark:  |
| Objectif 3 | :heavy_check_mark:  |
| Objectif 4 | :heavy_check_mark:  |
| Objectif 5 | :wavy_dash:         |
| Objectif 6 | :heavy_check_mark:  |

Legende:

+ :heavy_check_mark: : réussie

+ :wavy_dash: : fonctionne mais pas totalement

***

## **Informations supplémentaires**

- [ **1** ] Solution direct => usage de **moniteur** (**put** et **get** en __synchronized__)
    * Modélisation du problème en termes de variables et de méthodes
        * int in, out (le nombre de ressources messages entrées et sorties)
        * int nfull, nempty 
        * public get()
        * public put()

    * Tableau de Gardes-Actions:

    | Méthodes  | Pré-Action         | Gardes |Post-Action    |
    | :--------------- |:---------------|:-------|:-------|
    | put       | -       | nempty > 0    | buffer[in]=m; in=(in+1)%bufferSz;	nfull++; nbTotMsg++; |
    | get       | -       | nfull > 0     | Message msg=buffer[out]; out=(out+1)%bufferSz; nfull--; return msg; |
    | nmsg (nempty)  | -       | -       | return bufferSize - nfull; |
    | totmsg  | -       | -       | return nbTotMsg; |


- [ **2** ] Terminaison => class Terminaison 
    
    **class Terminaison** 

    * int nbProducerActif (initialisé dans le constructeur)

    * int nbConsumption (initialisé dans le constructeur)

    * Scénario terminé si `nbProducerActif = 0 && nbConsumption == 0`


- [ **3** ] Solution basée Sémaphore => usage de **3 sémaphores**

    **class ProdConsBuffer**

    * Sémaphore <span style="color: #26B260">notFull</span> qui est passant si le buffer n'est pas saturé
    
    ```Semaphore notFull = new Semaphore(this.bufferSz, true);```
    
    * Sémaphore <span style="color: #3296B2">notEmpty</span> qui est passant si le buffer n'est pas vide
    
    ```Semaphore notEmpty = new Semaphore(0, true);```

    * Sémaphore <span style="color: #B22600">mutex</span> qui met en place un "sas" à une seule place 
    
    ```Semaphore mutex = new Semaphore(1, true);```

- [ **4** ] Solution basée sur les Locks et Conditions de Java => Création de Lock

- [ **5** ] Multi-consommation => retour sur une solution direct avec l'usage de **moniteur** (**put** et **get** en __synchronized__) + nouvelle méthode get(k) dans la classe ProdConsBuffer

     **class ProdConsBuffer**

     * même principe que get, mais on initialise un tableau de k messages, puis on duplique le code de get dans get(k) dans une boucle for de 0 à k-1.
     (je n'appelle pas get dans get(k) car ils sont tous deux des méthodes synchronized, et l'imbrication de méthodes synchronized est à éviter.)

     Remarque: Nous avons également la possiblilité d'appeler get() dans get(k) si on enleve synchronized au get(), vu qu'on ne l'utilise plus dans cette version.
 
    **Mais**, on a également modifié le code de la méthode Terminaison.finish en ajoutant un test de terminaison. + quelques autres modifications dans Terminaison


:warning: **Problème d'affichage pour les dernières consommations**


- [ **6** ] Multi-exemplaires synchrone => retour sur une solution direct avec l'usage de **moniteur** (**put** et **get** en __synchronized__) + nouvelle méthode put(m, n) dans la classe ProdConsBuffer + upgrade la classe Message.

    **class Message** 

    * int nbExemplaire (initialisé dans le constructeur)

    * readExemplaire() -> nbExemplaire--

    * boolean readExemplaireTest() -> si nbExemplaire = 0 -> true sinon -> false

:warning: **Fonctionne sur le modèle 1 Producteur / N Consommateurs**

Exemple d'un cas du modèle N Producteur / N Consommateurs (**interblocage**):

```
    P2      P1      C1      C2      C3      |   
            |                               |   P1 produit 3 exemplaires du produit, puis se bloque jusqu'à ce que tous les exemplaires soient consommés
            --------|                       |   C1 consomme le 1er exemplaire du P1, puis se bloque jusqu'à ce que les 2 autres exemplaires du P1 soient consommés
                    --------|               |   C2 consomme le 2nd exemplaire du P1, puis se bloque jusqu'à ce que le dernier exemplaire du P1 soit consommé
    |                                       |   P2 produit 3 exemplaires du produit, puis se bloque jusqu'à ce que tous les exemplaires soient consommés
    ---------------------------------|      |   C3 consomme le 1er exemplaire du P2, puis se bloque jusqu'à ce que les 2 autres exemplaires du P2 soient consommés
                                     -      |   Tout le monde est bloqué
```
***