package ProdCons.v1_2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;

public class TestProdCons {
	
	public static void main(String[] args) throws InvalidPropertiesFormatException, IOException {
		Properties prop = new Properties();
		String PCdefault = "options.xml";
		String P1C1 = "P1C1_options.xml";
		String P1Cn = "P1Cn_options.xml";
		String PnC1 = "PnC1_options.xml";
		String PnCn = "PnCn_options.xml";
		
		
		prop.loadFromXML(TestProdCons.class.getClassLoader().getResourceAsStream(PnCn));

		int nProd = Integer.parseInt(prop.getProperty("nProd"));
		int nCons = Integer.parseInt(prop.getProperty("nCons"));
		int bufSz = Integer.parseInt(prop.getProperty("bufSz"));
		int prodTime = Integer.parseInt(prop.getProperty("prodTime"));
		int consTime = Integer.parseInt(prop.getProperty("consTime"));
		int minProd = Integer.parseInt(prop.getProperty("minProd"));
		int maxProd = Integer.parseInt(prop.getProperty("maxProd"));

		ArrayList prodList = new ArrayList<Producer>();
		ArrayList consList = new ArrayList<Consumer>();
		
		ProdConsBuffer pcBuf = new ProdConsBuffer(bufSz);
		
		Terminaison term = new Terminaison(nProd);
		
		System.out.println("#### INITIALISATION ####");
		System.out.println("Producteur: " + nProd);
		System.out.println("Consommateur: " + nCons);
		
		// Création des threads Producer
		for (int i=0; i<nProd; i++){
			prodList.add(new Producer(pcBuf, minProd, maxProd, prodTime, term));
		}
		
		// Création des threads Consumer
		for (int i=0; i<nCons; i++){
			consList.add(new Consumer(pcBuf, consTime, term));
		}
		
		int countProd = 0, countCons = 0;
		int x;
		System.out.println("##################  START  ##################");
		while(countProd != nProd || countCons != nCons) {
			Random rnd = new Random();
			Boolean coin = rnd.nextBoolean();
			if (countProd < nProd && coin) {  //Producer
				x=rnd.nextInt(nProd - countProd);
				((Thread) prodList.get(x)).start();
				prodList.remove(x);
				countProd++;
				//System.out.println("x: " + x + " & coin: " + coin);
			} else if (countCons < nCons) {	// Consumer
				x=rnd.nextInt(nCons - countCons);
				((Thread) consList.get(x)).start();
				consList.remove(x);				
				countCons++;
				//System.out.println("x: " + x + " & coin: " + coin);
			}
		}
	}
}
