package ProdCons.v1_2;

public class Message {
	
	private String contenu;
	
	public Message(String contenu) {
		this.contenu = contenu;
	}
	
	public String toString() {
		return contenu;
	}
}
