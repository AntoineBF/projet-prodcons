package ProdCons.v1_2;

public class Consumer extends Thread{

	private ProdConsBuffer buf;
	private int consTime;
	private Terminaison term;

	public Consumer(ProdConsBuffer b, int consTime, Terminaison term) {
		//this.id=id;
		this.buf=b;
		this.consTime=consTime;
		this.term=term;
		System.out.println("Consumer " + this.getId() + " initialisé");
	}
	
	public void run() {
		System.out.println("Consumer " + this.getId() + " commence !");
		while(true) {
			try {
				Message msg=buf.get();
				sleep(this.consTime);
				System.out.print("Start | consumer " + this.getId() + ": " + msg.toString() + "  ");
				this.term.consumption();
				System.out.println("	|||  End   | consumer " + this.getId() + ": " + msg.toString());
				this.term.terminedTest();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("thread" + this.getId() + ": finish\n");
	}
}