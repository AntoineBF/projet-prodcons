package ProdCons.v6;

public class Message {
	
	private String contenu;
	private int nbExemplaire;
	
	public Message(String contenu) {
		this.contenu = contenu;
		this.nbExemplaire = 0;
	}
	
	public Message(String contenu, int nbExp) {
		this.contenu = contenu;
		this.nbExemplaire = nbExp;
	}
	
	public void readExemplaire() {
		this.nbExemplaire--;
	}
	
	public int getNbExemplaire() {
		return this.nbExemplaire;
	}
	
	public boolean readExemplaireTest() {
		if (this.nbExemplaire == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public String toString() {
		return contenu;
	}
}
