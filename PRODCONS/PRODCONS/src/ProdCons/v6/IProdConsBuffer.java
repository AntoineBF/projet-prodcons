package ProdCons.v6;

public interface IProdConsBuffer {
	/**
	 * Put the message m in the buffer
	 */
	public void put(Message m) throws InterruptedException;

	/**
	 * Put n instannces of the message m in the buffer
	 * the current thread is blocked until all instances of the 
	 * message have been consumed. Any consumer of m is also blocked
	 * until all the instances of the message have been consumed.
	 */
	public void put(Message m, int n) throws InterruptedException;

	/**
	 * Retrieve a message from the buffer,
	 * following a FIFO order (if M1 was put before M2, M1
	 * is retrieved before M2)
	 */
	public Message get() throws InterruptedException;

	/**
	 * Retrieve k consecutive messages from the buffer,
	 * following a FIFO order (if M1 was put before M2, M1
	 * is retrieved before M2)
	 */
	public Message[] get(int k) throws InterruptedException;
	
	/**
	 * Return the number of messages currently available in
	 * the buffer
	 */
	public int nmsg();
	
	/**
	 * Return s the total number of messages that have
	 * been put in the buffer since its creation
	 */
	public int totmsg();
}