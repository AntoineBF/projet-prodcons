package ProdCons.v6;

public class Terminaison {
	public int nbProducerActif;
	int nbConsumption;
	
	public Terminaison(int nProd) {
		this.nbProducerActif = nProd;
		this.nbConsumption = 0;
	}

	public synchronized void finish() {
		this.nbProducerActif--;
		if(this.nbProducerActif == 0 && this.nbConsumption == 0) {
			System.out.println("-> Terminaison: VALID");
			System.out.println("FIN ProdCons.v6");
			System.exit(0);
		}
		//this.test();
	}

	public synchronized void incrConsumption() {
		this.nbConsumption++;
	}

	public synchronized void decrConsumption() {
		this.nbConsumption--;
		/*if(this.nbProducerActif == 0 && this.nbConsumption == 0) {
			System.out.println("-> Terminaison: VALID");
			System.out.println("FIN ProdCons.v6");
			System.exit(0);
		}*/
		//this.test();
	}
/*
	public void test() {  // nouveau terminedTest
		if(this.nbProducerActif == 0 && this.nbConsumption == 0) {
			System.out.println("-> Terminaison: VALID");
			System.out.println("FIN ProdCons.v6");
			System.exit(0);
		}
	}*/
	
}

