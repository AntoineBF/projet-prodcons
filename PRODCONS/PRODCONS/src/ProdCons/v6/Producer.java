package ProdCons.v6;

import java.util.Random;

public class Producer extends Thread{
	//private int id;
	private ProdConsBuffer buf;
	private int minProd;
	private int maxProd;
	private int prodTime;
	private int nbMsg;
	private int nCons;
	private Terminaison term;

	public Producer(ProdConsBuffer b, int minProd, int maxProd, int prodTime, Terminaison term, int nCons) { //int id, 
		//this.id=id;
		this.buf=b;
		this.maxProd=maxProd;
		this.minProd=minProd;
		this.prodTime=prodTime;
		this.nCons=nCons;
		this.term=term;
		
		Random rnd=new Random();
		this.nbMsg=rnd.nextInt(maxProd - minProd) + minProd;
		System.out.println("Producer " + this.getId() + " initialisé");
	}
	
	public void run() {
		System.out.println("Producer " + this.getId() + " commence !");		
		try {
			Random rnd=new Random();
			int x=1;
			//int x=2;
			for(int i=0; i<this.nbMsg; i++) {
				x=rnd.nextInt(this.nCons-1) + 1;
				//x=this.nCons;
				String msg = "Producer " + this.getId() + ": " + (i+1) + "/" + this.nbMsg; // + "(" + i + ")/"
				buf.put(new Message(msg, x), x);
				sleep(this.prodTime);
			}
			sleep(50);
			System.out.println("################## Producer" + this.getId() + ": Finish ################## \n");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.term.finish();
	}
}
