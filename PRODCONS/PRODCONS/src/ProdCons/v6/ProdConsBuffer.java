package ProdCons.v6;

/* Modélisation du problème en termes de variables et de méthodes
 * int in, out (le nombre de ressources messages entrées et sorties)
 * int nfull, nempty 
 * public get()
 * public put()
 * 
 * Définition du TGA
 * __________________________________________________________________________________________________________________
 * | Mtd			| Pré-Action	| Gardes 			| Post-Action												|
 * |----------------------------------------------------------------------------------------------------------------|
 * | put			| -				| nempty > 0 		| buffer[in]=m; in=(in+1)%bufferSz;	nfull++; nbTotMsg++;	|
 * | get			| -				| nfull > 0			| Message msg=buffer[out]; out=(out+1)%bufferSz; 			|
 * |                |               |                   | nfull--; return msg; 										|
 * | nmsg (nempty)	| -				| -					| return bufferSize - nfull;								|
 * | totmsg			| -				| -					| return nbTotMsg;											|
 * |----------------------------------------------------------------------------------------------------------------|
 */

public class ProdConsBuffer implements IProdConsBuffer{
	private Message[] buffer;
	private int in, out;
	private int bufferSz;
	private int nbMsg;  //nfull
	private int nbTotMsg;

	
	public ProdConsBuffer(int sizeBuf) {
		this.bufferSz=sizeBuf;
		this.in=0;
		this.out=0;
		this.nbMsg=0;
		this.nbTotMsg=0;
		this.buffer = new Message[this.bufferSz];
	}

	@Override
	public synchronized void put(Message m) throws InterruptedException {
		// TODO Auto-generated method stub
		try {
			while(!(this.nmsg() > 0)) {
				wait();
			}
			this.buffer[this.in]=m;
			
			this.in=(this.in+1)%this.bufferSz;
			
			this.nbMsg++;
			this.nbTotMsg++;

			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			notifyAll();
		}
	}

	@Override
	public synchronized void put(Message m, int n) throws InterruptedException {
		// TODO Auto-generated method stub
		/*for(int i=0; i<n;i++) {
			this.put(m);
		}*/
		for(int i=0; i<n;i++) {
			try {
				while(!(this.nmsg() > 0)) {
					wait();
				}
				this.buffer[this.in]=m;
				
				this.in=(this.in+1)%this.bufferSz;
				
				this.nbMsg++;
				this.nbTotMsg++;
	
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				notifyAll();
			}
		}
		//m.readExemplaire();
		while(!(m.readExemplaireTest())) {
			wait();
		}
		notifyAll();
	}

	@Override
	public synchronized Message get() throws InterruptedException {
		// TODO Auto-generated method stub
		while(!(this.nbMsg > 0)) {
			wait();
		}
		Message msg=this.buffer[this.out];
		this.out=(this.out+1)%this.bufferSz;
		
		msg.readExemplaire();
		this.nbMsg--;
		
		notifyAll();/*
		while(!msg.readExemplaireTest()) {
			wait();
		}*/
		return msg;
	}

	@Override
	public synchronized Message[] get(int k) throws InterruptedException {
		// TODO Auto-generated method stub
		Message[] msg = new Message[k];
		for(int i=0; i<k;i++) {
			//msg[i]=this.get();
			while(!(this.nbMsg > 0)) {
				wait();
			}
			msg[i]=this.buffer[this.out];
			this.out=(this.out+1)%this.bufferSz;
			
			this.nbMsg--;
			
			notifyAll();
		}
		return msg;
	}


	@Override
	public int nmsg() {	//nempty
		// TODO Auto-generated method stub
		return this.bufferSz-this.nbMsg;
	}

	@Override
	public int totmsg() {
		// TODO Auto-generated method stub
		return this.nbTotMsg;
	}
}
