package ProdCons.v6;

public class Consumer extends Thread{

	private ProdConsBuffer buf;
	private int consTime;
	private Terminaison term;

	public Consumer(ProdConsBuffer b, int consTime, Terminaison term) {
		//this.id=id;
		this.buf=b;
		this.consTime=consTime;
		this.term=term;
		
		this.setDaemon(true);
		
		System.out.println("Consumer " + this.getId() + " initialisé");
	}

	public void run() {
		System.out.println("Consumer " + this.getId() + " commence !");
		while(true) {
			try {
				Message msg=buf.get();
				sleep(this.consTime);
				System.out.print("	consumer " + this.getId() + ": " + msg.toString() + "  ");  //(" + msg.getNbExemplaire() + ")
				this.term.incrConsumption();
				System.out.println("	||| C" + this.getId() + ": " + msg.toString());
				this.term.decrConsumption();
				synchronized (this) {
					msg.readExemplaire();
					
					//attends que tous les exemplaires soient consommés si ce n'est pas déjà le cas
					while(msg.readExemplaireTest()) {
						wait();
					}
					notifyAll();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("thread" + this.getId() + ": finish\n");
	}
}