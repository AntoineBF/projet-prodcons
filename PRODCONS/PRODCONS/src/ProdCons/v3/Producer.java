package ProdCons.v3;

import java.util.Random;

public class Producer extends Thread{
	//private int id;
	private ProdConsBuffer buf;
	private int minProd;
	private int maxProd;
	private int prodTime;
	private int nbMsg;
	private Terminaison term;

	public Producer(ProdConsBuffer b, int minProd, int maxProd, int prodTime, Terminaison term) { //int id, 
		//this.id=id;
		this.buf=b;
		this.maxProd=maxProd;
		this.minProd=minProd;
		this.prodTime=prodTime;
		this.term=term;
		
		Random rnd=new Random();
		this.nbMsg=rnd.nextInt(maxProd - minProd) + minProd;
		System.out.println("Producer " + this.getId() + " initialisé");
	}
	
	public void run() {
		System.out.println("Producer " + this.getId() + " commence !");		
		try {
			for(int i=0; i<this.nbMsg; i++) {
				String msg = "Producer " + this.getId() + ": " + (i+1) + "/" + this.nbMsg; 
				buf.put(new Message(msg));
				sleep(this.prodTime);
			}
			System.out.println("\n################## Producer" + this.getId() + ": Finish ################## \n");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.term.finish();
	}
}
