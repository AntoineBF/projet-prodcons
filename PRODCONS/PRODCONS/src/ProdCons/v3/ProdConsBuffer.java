package ProdCons.v3;

import java.util.concurrent.Semaphore;

/* Modélisation du problème en termes de variables et de méthodes
 * int in, out (le nombre de ressources messages entrées et sorties)
 * int nfull, nempty 
 * public get()
 * public put()
 * 
 * Définition du TGA
 * __________________________________________________________________________________________________________________
 * | Mtd			| Pré-Action	| Gardes 			| Post-Action												|
 * |----------------------------------------------------------------------------------------------------------------|
 * | put			| -				| nempty > 0 		| buffer[in]=m; in=(in+1)%bufferSz;	nfull++; nbTotMsg++;	|
 * | get			| -				| nfull > 0			| Message msg=buffer[out]; out=(out+1)%bufferSz; 			|
 * |                |               |                   | nfull--; return msg; 										|
 * | nmsg (nempty)	| -				| -					| return bufferSize - nfull;								|
 * | totmsg			| -				| -					| return nbTotMsg;											|
 * |----------------------------------------------------------------------------------------------------------------|
 */

public class ProdConsBuffer implements IProdConsBuffer{
	private Message[] buffer;
	private int in, out;
	private int bufferSz;
	private int nbMsg;  //nfull
	private int nbTotMsg;
	Semaphore notFull;
	Semaphore notEmpty;

	Semaphore mutex;
	
	public ProdConsBuffer(int sizeBuf) {
		this.bufferSz=sizeBuf;
		this.in=0;
		this.out=0;
		this.nbMsg=0;
		this.nbTotMsg=0;
		this.buffer = new Message[this.bufferSz];
		
		// Passant si le buffer n'est pas saturé
		this.notFull = new Semaphore(this.bufferSz, true);
		// Passant si le buffer n'est pas vide
		this.notEmpty = new Semaphore(0, true);
		// Mise en place du sas à une seule place
		this.mutex = new Semaphore(1, true);
	}

	@Override
	public void put(Message m) throws InterruptedException {
		// TODO Auto-generated method stub
		try {
			this.notFull.acquire();
			this.mutex.acquire();
			
			this.buffer[this.in]=m;
			this.in=(this.in+1)%this.bufferSz;
			this.nbMsg++;
			this.nbTotMsg++;
		} finally {
			this.mutex.release();
			this.notEmpty.release();
		}
	}

	@Override
	public Message get() throws InterruptedException {
		// TODO Auto-generated method stub
		try {
			this.notEmpty.acquire();
			this.mutex.acquire();
			
			Message msg=this.buffer[this.out];
			this.out=(this.out+1)%this.bufferSz;
			this.nbMsg--;
			return msg;
		} finally {
			this.mutex.release();
			this.notFull.release();
		}
	}

	@Override
	public int nmsg() {	//nempty
		// TODO Auto-generated method stub
		return this.bufferSz-this.nbMsg;
	}

	@Override
	public int totmsg() {
		// TODO Auto-generated method stub
		return this.nbTotMsg;
	}
}
