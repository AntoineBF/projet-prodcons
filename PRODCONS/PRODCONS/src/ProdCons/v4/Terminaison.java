package ProdCons.v4;

public class Terminaison {
	public int nbProducerActif;
	int nbConsumption;
	
	public Terminaison(int nProd) {
		this.nbProducerActif = nProd;
		this.nbConsumption = 0;
	}

	public synchronized void finish() {
		this.nbProducerActif--;
	}

	public synchronized void consumption() {
		this.nbConsumption++;
	}

	public synchronized void terminedTest() {
		this.nbConsumption--;
		if(this.nbProducerActif == 0 && this.nbConsumption == 0) {
			System.out.println("-> Terminaison: VALID");
			System.out.println("FIN ProdCons.v4");
			System.exit(0);
		}
		
	}
	
}
