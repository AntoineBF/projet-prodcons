package ProdCons.v4;

import java.util.concurrent.locks.*;

/* Modélisation du problème en termes de variables et de méthodes
 * int in, out (le nombre de ressources messages entrées et sorties)
 * int nfull, nempty 
 * public get()
 * public put()
 * 
 * Définition du TGA
 * __________________________________________________________________________________________________________________
 * | Mtd			| Pré-Action	| Gardes 			| Post-Action												|
 * |----------------------------------------------------------------------------------------------------------------|
 * | put			| -				| nempty > 0 		| buffer[in]=m; in=(in+1)%bufferSz;	nfull++; nbTotMsg++;	|
 * | get			| -				| nfull > 0			| Message msg=buffer[out]; out=(out+1)%bufferSz; 			|
 * |                |               |                   | nfull--; return msg; 										|
 * | nmsg (nempty)	| -				| -					| return bufferSize - nfull;								|
 * | totmsg			| -				| -					| return nbTotMsg;											|
 * |----------------------------------------------------------------------------------------------------------------|
 */

public class ProdConsBuffer implements IProdConsBuffer{
	private Message[] buffer;
	private int in, out;
	private int bufferSz;
	private int nbMsg;  //nfull
	private int nbTotMsg;
	Condition prodCond;
	Condition consCond;
	Lock lock;
	
	public ProdConsBuffer(int sizeBuf) {
		this.bufferSz=sizeBuf;
		this.in=0;
		this.out=0;
		this.nbMsg=0;
		this.nbTotMsg=0;
		
		this.buffer = new Message[this.bufferSz];
		
		this.lock = new ReentrantLock();
		this.prodCond = lock.newCondition();
		this.consCond = lock.newCondition();
	}

	@Override
	public void put(Message m) throws InterruptedException {
		// TODO Auto-generated method stub
		this.lock.lock();
		try {
			while(!(this.nmsg() > 0)) {
				this.prodCond.await();
			}
			this.buffer[this.in]=m;
			
			this.in=(this.in+1)%this.bufferSz;
			
			this.nbMsg++;
			this.nbTotMsg++;
			this.consCond.signal();
		} finally {
			this.lock.unlock();
		}
	}

	@Override
	public Message get() throws InterruptedException {
		// TODO Auto-generated method stub
		this.lock.lock();
		try {
			while(!(this.nbMsg > 0)) {
				this.consCond.await();
			}
			Message msg=this.buffer[this.out];
			this.out=(this.out+1)%this.bufferSz;

			this.nbMsg--;			
			this.prodCond.signal();
			return msg;
		} finally {
			this.lock.unlock();
		}
	}

	@Override
	public int nmsg() {	//nempty
		// TODO Auto-generated method stub
		return this.bufferSz-this.nbMsg;
	}

	@Override
	public int totmsg() {
		// TODO Auto-generated method stub
		return this.nbTotMsg;
	}
}
