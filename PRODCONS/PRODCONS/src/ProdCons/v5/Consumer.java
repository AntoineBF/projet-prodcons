package ProdCons.v5;

import java.util.Random;

public class Consumer extends Thread{

	private ProdConsBuffer buf;
	private int consTime;
	private Terminaison term;
	
	private Random rnd;
	private int xMax=5;

	public Consumer(ProdConsBuffer b, int consTime, Terminaison term) {
		//this.id=id;
		this.buf=b;
		this.consTime=consTime;
		this.term=term;
		
		this.rnd=new Random();
		
		this.setDaemon(true);
		
		System.out.println("Consumer " + this.getId() + " initialisé");
	}
	
	public void run() {
		System.out.println("Consumer " + this.getId() + " commence !");
		while(true) {
			try {
				int x=rnd.nextInt(this.xMax) + 1;
				Message[] msg=buf.get(x);
				sleep(this.consTime);
				System.out.println("Start | ");
				for(int i=0; i<x;i++) {
					System.out.print("	consumer " + this.getId() + "(" + (x-i) + "): " + msg[i].toString() + "  ");
					this.term.incrConsumption();
					System.out.println("	|||  End   | consumer " + this.getId() + ": " + msg[i].toString());
					this.term.decrConsumption();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}