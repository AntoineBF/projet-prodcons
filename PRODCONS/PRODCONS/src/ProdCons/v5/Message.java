package ProdCons.v5;

public class Message {
	
	private String contenu;
	
	public Message(String contenu) {
		this.contenu = contenu;
	}
	
	public String toString() {
		return contenu;
	}
}
